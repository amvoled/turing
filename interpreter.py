# Interpreter.py, part of the turing machine project
# Created by Guillaume Ducrocq

# This function interpret an instruction to the memory
# An instruction is an array, refer to the manual

import sys

import glob

def inter(inst: list, memory: list):
    if (len(inst) != 3):
        sys.exit("FATAL : Invalid instruction")
    if (inst[1] != ''):
        memory[glob.mem_index] = inst[0]
    if (inst[2] == 1):
        glob.mem_index += 1
    elif (inst[2] == -1):
        glob.mem_index -= 1
    if (len(memory) <= glob.mem_index):
        memory.append(0)
    if (glob.mem_index < 0):
        glob.mem_index = 0
        memory.insert(0, 0)
    if (inst[3] != -1):
        glob.state = inst[3]
