# Parser.py, part of the turing machine project
# Created by Guillaume Ducrocq

# This function returns an array of the states

import re
import sys

import glob

V_VARS = ["STEPLIMIT", "SIZELIMIT", "DISPLAY", "STARTSTATE"]
NUM_PAT = re.compile("^\[\d+\]$")

def parse(filename: str, program: list):
    linput = "WFI"
    d_state = []
    try:
        fptr = open(filename)
    except:
        print ("Could not open file : " + filename)
        return program
    print ("Starting the parsing of " + filename)
    line = 0
    for linput in fptr:
        line = line + 1
        if (linput[0] == '\n') or (linput[0] == '#'):
            continue
        commands = linput.split()
        if (commands[0] in V_VARS) and (len(commands) == 2):
            parse_vars(commands)
        elif (commands[0][:2] == '$$') and (len(commands) == 2):
            if (len(commands[0]) <= 2):
                print ("Trying to declare a custom variable without a name on line " + str(line))
                continue
            if (commands[0] in glob.uservarslabels):
                print ("Trying to reassign an existing user variable on line " + str(line))
            elif (commands[0] == "$$DEFAULT"):
                print ("Trying to reassign the DEFAULT variable on line " + str(line))
            else:
                glob.uservarslabels.append(commands[0])
                glob.uservarsvalues.append(commands[1])
        elif (NUM_PAT.match(commands[0]) and len(commands) == 1):
            cstate = int(commands[0][1:-1])
            if cstate in d_state:
                sys.exit ("FATAL : Multiple definition of state " + int(cstate))
            d_state.append(cstate)
            print ("Entering state " + str(cstate))
            line = parse_state(fptr, line)
            print ("Exiting state")
        else:
            print ("What is " + str(commands) + " ?")
    print ("Parsing of file " + filename + " completed")

def parse_vars(commands: list):
    try:
        if (commands[0] == "STEPLIMIT"):
            glob.steplimit = int(commands[1])
        elif (commands[0] == "SIZELIMIT"):
            glob.sizelimit = int(commands[1])
        elif (commands[0] == "DISPLAY"):
            glob.display = int(commands[1])
        elif (commands[0] == "STARTSTATE"):
            glob.state = int(commands[1])
    except:
        print ("Variable set must be a number")

def parse_state(fptr, linenbr: int):
    line = fptr.readline()
    linenbr = linenbr + 1
    state = []
    if (line != "{\n"):
        sys.exit ("FATAL : Line " + str(linenbr) + " : Syntax error in state definition")
    line = fptr.readline()
    linenbr = linenbr + 1
    while (line != "}\n" and line != "}"):
        if (line[0] == '\n') or (line[0] == '#'):
            line = fptr.readline()
            linenbr = linenbr + 1
            continue
        if (line == ""):
            sys.exit ("FATAL : Line " + str(linenbr) + " : Unexpected EOF")
        tempc = ['', '', 0, -1]
        comm = line.split()
        print (comm)
        i = 0
        linesize = len(comm)
        if (linesize % 2 != 0):
            sys.exit ("FATAL : Line " + str(linenbr) + " : Syntax error, odd tab")
        else:
            while (i < linesize):
                if (comm[i] == "IF"):
                    if (comm[i+1][:2] == "$$"):
                        if (len(comm[i+1]) >= 3):
                            if not (comm[i+1] in glob.uservarslabels):
                                sys.exit("FATAL : The user variable does not exist on line " + str(linenbr))
                            else:
                                tempc[0] = glob.uservarsvalues[glob.uservarslabels.index(comm[i+1])]
                        else:
                            sys.exit("FATAL : The user variable name is invalid on line " + str(linenbr))
                    else:
                        tempc[0] = comm[i + 1]
                elif (comm[i] == "WRITE"):
                    if (len(comm[i + 1]) == 1):
                        tempc[1] = comm[i + 1]
                    else:
                        sys.exit ("FATAL : Line " + str(linenbr) + " : Write must be followed by ONE symbol")
                elif (comm[i] == "GO"):
                    if (comm[i + 1] == "LEFT"):
                        tempc[2] = -1
                    elif (comm[i + 1] == "RIGHT"):
                        tempc[2] = 1
                    else:
                        sys.exit ("FATAL : Line " + str(linenbr) + " : GO must be followed by LEFT or RIGHT")
                elif (comm[i] == "STATE"):
                    try:
                        tempo = int(comm[i + 1])
                        if (tempo < 0):
                            raise
                    except:
                        sys.exit ("FATAL : Line " + str(linenbr) + " : STATE must be followed by a positive number")
                    tempc[3] = tempo
                i = i + 2
            state.append(tempc)
        line = fptr.readline()
        linenbr = linenbr + 1
    print (state)
    return linenbr
