def init():
    global mem_index
    global state
    global steplimit
    global sizelimit
    global display
    global uservarslabels
    global uservarsvalues
    global memory
    global program

    mem_index = 0
    state = 0
    steplimit = 100
    sizelimit = 100
    display = 0
    uservarslabels = []
    uservarsvalues = []
    memory = []
    program = []
